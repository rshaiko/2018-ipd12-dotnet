﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstDB
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\!!!C_Sharp_Rep\b_FirstDB\FirstDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
            // insert a records
            SqlCommand insertCommand = new SqlCommand("INSERT INTO Friends (Name) VALUES (@Name)", conn);
            insertCommand.Parameters.AddWithValue("@Name", "Timmy");
            insertCommand.ExecuteNonQuery();
            // select all recors
            SqlCommand command = new SqlCommand("SELECT * FROM Friends", conn);
            using (SqlDataReader reader = command.ExecuteReader())
            {                
                while (reader.Read())
                {
                    Console.WriteLine(String.Format("{0}: {1}", reader[0], reader[1]));
                }
            }   
            //
            Console.ReadLine();
        }
    }
}
