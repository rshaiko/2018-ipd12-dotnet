﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace a_ScoopSelector
{

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            if (lbFlavours.SelectedIndex == -1) {
                //MessageBox.Show("Please select a scoop");
                return;
            }
            String tmp = ((ListBoxItem)lbFlavours.SelectedValue).Content.ToString();
            lbSelected.Items.Add(tmp);
            lbFlavours.SelectedIndex = -1;
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lbSelected.SelectedIndex == -1)
            {
                return;
            }
            lbSelected.Items.RemoveAt(lbSelected.SelectedIndex);
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            lbSelected.Items.Clear();
        }
    }
}
