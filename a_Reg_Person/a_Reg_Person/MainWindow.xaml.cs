﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace a_Reg_Person
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Register();
        }

        private void Register()
        {
            int age;
            String name, gender, pets, continent;

            try
            {
                //NAME
                name = tbName.Text;
                if (name.Length<2 || name.Length > 50)
                {
                    throw new InvalidDataException("Name must be of 2-50 symbols");
                }

                //AGE
                if (int.TryParse(tbAge.Text, out age))
                {
                    if (age < 0 || age > 150)
                    {
                        throw new InvalidDataException("Age value must be 0-150");
                    }
                }
                else
                {
                    throw new InvalidDataException("Please enter correct age value");
                }

                // GENDER
                if (rbM.IsChecked == true) { gender = "Male"; }
                else if (rbF.IsChecked == true) { gender = "Female"; }
                else if (rbNa.IsChecked == true) { gender = "N/A"; }
                else
                {
                    throw new InvalidDataException("Gender is not selected");
                }

                //PETS
                pets = "";
                List <string> petsList = new List<string>();

                if (cbCat.IsChecked == true)
                {
                    petsList.Add("Cat(s)");
                }
                if (cbDog.IsChecked == true)
                {
                    petsList.Add("Dog(s)");
                }
                if (cbOther.IsChecked == true)
                {
                    petsList.Add("Other");
                }

                pets = string.Join(",", petsList);

                //CONTINENT
                // continent = (cbbContinent.SelectedItem as ComboBoxItem).Content.ToString();
                // NullReferenceException
                continent = cbbContinent.SelectionBoxItem.ToString();
                if (continent.Equals(""))
                { 
                    throw new InvalidDataException("Please select a continent");
                }
                String line = String.Join(";", name, age, gender, pets, continent);
                try
                {
                    File.AppendAllText(@"..\..\..\data.txt", line+"\n");
                }
                catch(Exception e)
                {
                    throw new InvalidDataException("Error: Information was not saved");
                }

                MessageBox.Show("Entry was stored to the file");
                foreach (Control ctl in Gr.Children)
                {
                    if (ctl.GetType() == typeof(CheckBox))
                        ((CheckBox)ctl).IsChecked = false;
                    if (ctl.GetType() == typeof(TextBox))
                        ((TextBox)ctl).Text = String.Empty;
                    if (ctl.GetType() == typeof(RadioButton))
                        ((RadioButton)ctl).IsChecked = false;
                    if (ctl.GetType() == typeof(ComboBox))
                        ((ComboBox)ctl).SelectedIndex=-1;
                }
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
