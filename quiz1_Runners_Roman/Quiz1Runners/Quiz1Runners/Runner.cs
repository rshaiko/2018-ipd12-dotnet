﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz1Runners
{
    class Runner
    {
        public string Name; // 2-20 characters long
        public double AvgTime; // 0 or more
        public List<double> RuntimesList = new List<double>(); // keep as field, not property

        public Runner(string name, double avgTime, List<double> runtimesList)
        {
            mName = name;
            mAvgTime = avgTime;
            RuntimesList = runtimesList;
        }

        public string mName
        {
            get
            {
                return Name;
            }
            set
            {
                string rex = @"^[A-Za-z0-9 .-]{1,20}$";
                Regex r = new Regex(rex);
                Match m = r.Match(value);
                if (m.Success)
                {
                    Name = value;
                }
                else
                {
                    throw new InvalidDataException("Name has to be 2-20 letters long");
                }
            }
        }

        public double mAvgTime
        {
            get
            {
                return AvgTime;
            }
            set
            {
                if (value >= 0)
                {
                    AvgTime = value;
                }
                else
                {
                    throw new InvalidDataException("Run can not be less than zero!");
                }
                
            }
        }

    }

    
}
