﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz1Runners
{
    class Program
    {
        static void Main(string[] args)
        {
            string currentRunner="";
            double currentRun;
            List<double> runs;
            Dictionary<string, List<double>> dRunners = new Dictionary<string, List<double>>();
            List <Runner> runnersList = new List<Runner>();
            string rex = "[a-zA-Z]"; // to check whether line contains letters
            try
            {
                foreach (string line in File.ReadLines("runners.txt"))
                {
                    if (!line.Equals(""))
                    {
                        //check whether line doesn't contain letters - REGEX
                        Regex r = new Regex(rex);
                        Match m = r.Match(line);
                        if (m.Success)
                        {
                            //get or add new runners and add run to it
                            if (dRunners.TryGetValue(line, out runs))
                            {
                                currentRunner = line; // in case name of the runner will repeat in the file 
                            }
                            else
                            {
                                //adding new runner
                                dRunners.Add(line, new List<double> { });
                                currentRunner = line;
                            }
                        }
                        else
                        {
                            //it is probably run so we parse double
                            if (double.TryParse(line, out currentRun))
                            {
                                //add result to runners
                                if (dRunners.TryGetValue(currentRunner, out runs))
                                {
                                    runs.Add(currentRun); //adding a run to currentRunner
                                    dRunners[currentRunner] = runs;   //updating dict value
                                }
                                else
                                {
                                    throw new InvalidDataException("Runner was not found");
                                }

                            }
                            else
                            {
                                throw new InvalidDataException("Run time was not a double");
                            }

                        }
                    }
                    else
                    {
                        throw new InvalidDataException("Empty line was skipped");
                    }
                }
                
                //Populating of the runnersList
                double avgTime;
                List <double> runtimeList;
                foreach (KeyValuePair<string, List<double>> runner in dRunners)
                {
                    avgTime = 0;
                    string name = runner.Key;
                    runtimeList = runner.Value;
                    foreach (double run in runtimeList) {
                        avgTime += run;
                    }
                    avgTime = avgTime / runtimeList.Count;
                    runnersList.Add(new Runner(name, avgTime, runtimeList)); 
                }
            }
            catch (InvalidDataException ex)
            {
                Console.WriteLine("\nError: wrong data!\n{0}\n", ex.Message);
            }

            foreach (Runner r in runnersList)
            {
                Console.WriteLine("{0} ran {1} times",r.Name, r.RuntimesList.Count);
            }
            foreach (Runner r in runnersList)
            {
                Console.WriteLine("{0}'s average is {1:.00}",r.Name, r.AvgTime);
            }
            Console.WriteLine("\nThere were {0} runners in total\n", runnersList.Count);

            //LINQ
            var listByName = from r in runnersList orderby r.Name select r;
            Console.WriteLine("==LINQ sorting by name==");
            foreach (Runner r in listByName)
            {
                Console.WriteLine("{0}'s average is {1:.00}", r.Name, r.AvgTime);
            }

            Console.ReadKey();
        }
    }
}
