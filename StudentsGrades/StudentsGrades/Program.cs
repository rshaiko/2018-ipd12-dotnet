﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsGrades
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] splitName;
            string[] splitGrades;
            double grade;
            double sumGrade;

            try
            {
                foreach (string line in File.ReadLines(@"..\..\..\grades.txt")) 
                {
                    sumGrade = 0;
                    splitName = line.Split(new char[]{':'});
                    splitGrades = splitName[1].Split(new char[] {','});
                    foreach (string g in splitGrades) {
                        grade = letterToNumberGrade(g);
                        sumGrade += grade;
                    }
                    Console.WriteLine("{0} has GPA {1:.00}", splitName[0], sumGrade / splitGrades.Length);
                }
            }
            catch(IOException ex){
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            Console.ReadKey();
        }


        private static double letterToNumberGrade(string strGrade)
        {
            double g;
            switch (strGrade)
            {
                case "A":
                    return 4.00;
                case "A-":
                    g = 3.67;
                    break;
                case "B+":
                    g = 3.33;
                    break;
                case "B":
                    g = 3.00;
                    break;
                case "B-":
                    g = 2.67;
                    break;
                case "С+":
                    g = 2.33;
                    break;
                case "С":
                    g = 2.00;
                    break;
                case "С-":
                    g = 1.67;
                    break;
                case "D+":
                    g = 1.33;
                    break;
                case "D":
                    g = 1.00;
                    break;
                case "D-":
                    g = 0.67;
                    break;
                case "F":
                    g = 0;
                    break;
                default:
                    g = 0;
                    break;
            }
            return g;
        }
    }
}
