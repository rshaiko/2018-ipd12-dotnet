﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;



namespace EFNewPeople
{

    public partial class MainWindow : Window
    {
        static List<Person> people = new List<Person>();
        Database ctx;

        public MainWindow()
        {
            InitializeComponent();
            try
            {

                ctx = new Database();
            }
            catch(SqlException ex)
            {
                MessageBox.Show(ex.Message);
                System.Environment.Exit(1);
            }
            Person p = new Person {Name = "asd", Age = 3, Height = 180};
            ctx.People.Add(p); // persist - schedule Person to be inserted into database
            ctx.SaveChanges();
            Console.WriteLine("Person persisted");



            lvPeople.ItemsSource = ctx.GetPeople();
        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1)
            {
                return;
            }
            //Person p = (Person)lvPeople.SelectedValue;
            //lblId.Content = p.Id;
            //tbName.Text = p.Name;
            //tbAge.Text = p.Age + "";
            //slHeight.Value = p.Height;
        }

        private void cmRemove_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1)
            {
                return;
            }
            //Person p = (Person)lvPeople.SelectedValue;
            //db.deletePerson(p.Id);
            //lvPeople.ItemsSource = db.GetPeople();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1)
            {
                return;
            }
            try
            {
                //Person p = (Person)lvPeople.SelectedValue;
                //p.Name=tbName.Text;
                //p.Height = (float)slHeight.Value;
                //p.Age = int.Parse(tbAge.Text);
                //db.updatePerson(p);
            }
            catch (SqlException ex) {
                MessageBox.Show(ex.Message);
                return;
            }
            lvPeople.ItemsSource = ctx.GetPeople();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            float height = (float)(slHeight.Value);
            //if (int.TryParse(tbAge.Text, out int age))
            //{
            //    try
            //    {
            //        db.addPerson(new Person(name, age, height));
            //    }
            //    catch (SqlException er)
            //    {
            //        MessageBox.Show(er.Message);
            //        return;
            //    }
            //}
            lvPeople.ItemsSource = ctx.GetPeople();
        }

        private void slHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            
            {
                try { tfHeight.Text = slHeight.Value.ToString(); }
                catch (NullReferenceException ) { }
            }
        }
    }
}
