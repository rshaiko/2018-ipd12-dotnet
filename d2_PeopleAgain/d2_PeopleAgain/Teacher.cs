﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace d2_PeopleAgain
{
    public class Teacher : Person
    {
        int mYearsOfExperience; // 0-100
        string mSubject; //1-20 characters long

        public string Subject
        {
            get => mSubject;
            
            set
            {
                string rex = @"^[A-Za-z ]{1,20}$";
                Regex r = new Regex(rex);
                Match m = r.Match(value);
                if (m.Success)
                {
                    mSubject = value;
                }
                else
                {
                    throw new InvalidDataException("Subject name has to be 1-21 letters including spaces");
                }
            }
        }
        public int YearsOfExperience {


            get => mYearsOfExperience;
            set
            {
                if (value >= 0 && value <= 100)
                {
                    mYearsOfExperience = value;
                }
                else
                {
                    throw new InvalidDataException("GPA has to be between 0 and 4.3");
                }
                
            }
        }
        public string MSubject { get => mSubject; set => mSubject = value; }

        public Teacher (string name, int age, string subject, int yearsOfExperience) : base (name, age)
        {
            YearsOfExperience = yearsOfExperience;
            Subject = subject;
        }

        override public string ToString()
        {
            return base.ToString()+string.Format(" \tSubject: {0} \tExperience: {1}", mSubject, mYearsOfExperience);
        }
    }
}
