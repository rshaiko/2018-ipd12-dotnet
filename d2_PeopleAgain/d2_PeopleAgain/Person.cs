﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace d2_PeopleAgain
{
    public class Person
    {
        private string mName;
        private int mAge;

        public string Name {
            get
            {
                return mName;
            }
            set
            {
                string rex = @"^[A-Za-z .-]{1,20}$";
                Regex r = new Regex(rex);
                Match m = r.Match(value);
                if (m.Success)
                {
                    mName = value;
                }
                else
                {
                    throw new InvalidDataException("Name has to be 1-21 letters including dots, dashes, and spaces");
                }
            }
        }

        public int Age
        {
            get
            {
                return mAge;
            }
            set
            {
                if (int.TryParse(value+"", out int Ok)) {
                    if (Ok >= 1 && Ok <= 150)
                    {
                        mAge = value;
                        return;
                    }
                }
                throw new InvalidDataException("Age has to be an integer between 1 and 150");
            }
        }

        public Person(string name, int age) {
            Name = name;
            Age = age;
        }



        override public string ToString()
        {
            return string.Format("Name: {0} \tAge: {1}", mName, mAge);
        }
    }
}
