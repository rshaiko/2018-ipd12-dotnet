﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace d2_PeopleAgain 
{
     public class Student : Person
    {
        double mGPA; // 0-4.3
        string mProgram; // 1-20 char

        public double GPA {
            get => mGPA;
            set
            {
                if (value >= 0 && value <= 4.3)
                {
                    mGPA = value;
                }
                else
                {
                    throw new InvalidDataException("GPA has to be between 0 and 4.3");
                }
            }
        }


        public string Program { get => mProgram;
            set
            {
                string rex = @"^[A-Za-z ]{1,20}$";
                Regex r = new Regex(rex);
                Match m = r.Match(value);
                if (m.Success)
                {
                    mProgram = value;
                }
                else
                {
                    throw new InvalidDataException("Program name has to be 1-21 letters including spaces");
                }
            }
        }


        public Student (string name, int age, string program, double gpa) : base (name, age)
        {
            GPA = gpa;
            Program = program;
        }

        override public string ToString() {
            return base.ToString() + string.Format(" \tProgram: {0} \tGPA: {1:.00}", mProgram, mGPA);
        }
    }
}
