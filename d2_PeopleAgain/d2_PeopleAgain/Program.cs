﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d2_PeopleAgain
{
    class Program
    {
        static List <Person> PeopleList = new List<Person>();

        static void Main(string[] args)
        {
            string [] splitType;
            string[] splitData;

            string name, subj, prog;
            int age, experience;
            double gpa;
            foreach (string line in File.ReadLines(@"..\..\..\people.txt"))
            {
                try
                {
                    if (!line.Equals(""))
                    {
                        splitType = line.Split(new char[] { ':' });
                        switch (splitType[0])
                        {
                            
                            case "Person":
                                splitData = splitType[1].Split(new char[] { ',' });
                                name = splitData[0];
                                if (!int.TryParse(splitData[1], out age)){
                                    throw new InvalidDataException ("Age has to be an integer");
                                }
                                PeopleList.Add(new Person(name, age));
                                break;


                            case "Student":
                                splitData = splitType[1].Split(new char[] { ',' });
                                name = splitData[0];
                                if (!int.TryParse(splitData[1], out age))
                                {
                                    throw new InvalidDataException("Age has to be an integer");
                                }
                                if (!double.TryParse(splitData[2], out gpa))
                                {
                                    throw new InvalidDataException("GPA has to be a double");
                                }
                                prog = splitData[3];
                                PeopleList.Add(new Student(name, age, prog, gpa));
                                break;


                            case "Teacher":
                                splitData = splitType[1].Split(new char[] { ',' });
                                name = splitData[0];
                                if (!int.TryParse(splitData[1], out age))
                                {
                                    throw new InvalidDataException("Age has to be an integer");
                                }
                                subj = splitData[2];
                                if (!int.TryParse(splitData[3], out experience))
                                {
                                    throw new InvalidDataException("Years of experience has to be a integer");
                                }
                                PeopleList.Add(new Teacher(name, age, subj, experience));
                                break;

                            default:
                                throw new InvalidDataException (string.Format("Type {0} not found", splitType[0]));
                        }
                        continue;
                    }
                }
                catch (InvalidDataException ex)
                {
                    Console.WriteLine("\nError: Line \"{0}\" was skipped - wrong data!\n{1}\n", line, ex.Message);
                }

            }
            foreach (Person pp in PeopleList) {
                Console.WriteLine(pp);
            }

            Console.WriteLine("\nSort by name DESC: \n");
            PeopleList.Sort((x, y) => y.Name.CompareTo(x.Name));
            foreach (Person pp in PeopleList) {
                Console.WriteLine(pp);
            }

            Console.WriteLine("\nSort by age DESC: \n");
            PeopleList.Sort((x, y) => y.Age.CompareTo(x.Age));
            foreach (Person pp in PeopleList)
            {
                Console.WriteLine(pp);
            }

            Console.ReadKey();
        }
    }
}
