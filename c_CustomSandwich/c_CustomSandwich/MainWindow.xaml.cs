﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace c_CustomSandwich
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btMake_Click(object sender, RoutedEventArgs e)
        {
            CustomDialog dialog = new CustomDialog();
            dialog.ShowDialog(); //execution will block here in this method until the popup closes
            if (dialog.DialogResult!=true)
            {
                return;
            }
            lblBread.Content = dialog.bread;
            lblVeggies.Content = dialog.veggies;
            lblMeat.Content = dialog.meat;
        }

    }
}
