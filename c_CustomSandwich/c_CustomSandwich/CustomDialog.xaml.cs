﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace c_CustomSandwich
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {
        public string bread
        {
            get
            {
                if (cbbBread.SelectedIndex == -1) return string.Empty;
                return cbbBread.SelectionBoxItem.ToString();
            }
        }

        public string veggies
        {
            get
            {
                string vegs = "";
                List<string> vegList = new List<string>();

                if (cbVegLettuce.IsChecked == true)
                {
                    vegList.Add("Lettuce");
                }
                if (cbVegTomatoes.IsChecked == true)
                {
                    vegList.Add("Tomatoes");
                }
                if (cbVegCucumbers.IsChecked == true)
                {
                    vegList.Add("Cucumbers");
                }
                vegs = string.Join(", ", vegList);
                return vegs;
            }
        }

        public string meat
        {
            get
            {
                string meat = "";
                if (rbChicken.IsChecked == true) { meat = "Chicken"; }
                else if (rbTurkey.IsChecked == true) { meat = "Turkey"; }
                else if (rbTofu.IsChecked == true) { meat = "Tofu"; }
                return meat;
            }
        }

        public CustomDialog()
        {
            InitializeComponent();
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            if (cbbBread.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a bread");
                return;
            }
            this.DialogResult = true;
        }
    }
}
