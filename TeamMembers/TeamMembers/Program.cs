﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamMembers
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] splitName;
            string[] splitMembers;
            bool exist;
            Dictionary<string, List <string> > dict = new Dictionary<string, List<string> >();
            foreach (string line in File.ReadLines("..\\..\\..\\input.txt"))
            {
                splitName = line.Split(new char[] { ':' });
                splitMembers = splitName[1].Split(new char[] { ',' });
                List<string> curSet;
                foreach (string m in splitMembers)
                {
                    // if key (name) already exists
                    if (exist = dict.TryGetValue(m, out curSet))
                    {
                        curSet.Add(splitName[0]);   //adding to this name the team
                        dict[m] = curSet;   //updating dict value
                    }
                    else
                    {
                        //create key (name) and add current team
                        dict.Add(m, new List<string> { splitName[0] } );
                    }
                }
            }
            //Output
            foreach (KeyValuePair<string, List<string>> k in dict)
            {
                string teams = string.Join(", ", k.Value);
                Console.WriteLine("{0} plays in: {1}", k.Key, teams);
            }
            Console.ReadKey();
        }
    }
}
