﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d01_HomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            int number;
            while(true)
            {
                Console.WriteLine("Enter a positive integer, 0 to end:");

                if (!int.TryParse(Console.ReadLine(), out number))
                {
                    Console.WriteLine("Wrong input! Try again.");
                    continue;
                }

                if (number < 1)
                {
                    break;
                }
                list.Add(number);
            }
            int sum=0;
            Console.Write("Avg of entered numbers: ");
            foreach (int i in list) {
                sum += i;
            }
            Console.WriteLine((double)sum / list.Count);

            list.Sort();
            int cou = list.Count;
            Console.WriteLine("Maximum is {0}", list[cou-1]);

            Console.Write("The median is: ");
            if (list.Count % 2 == 1) {
                Console.WriteLine(list[(cou - 1) / 2]);
            }
            else {
                Console.WriteLine((double)(list[cou/2]+list[cou/2-1])/2);
            }
            //Write ti the file
            string data = String.Join(";", list);
            try
            {
                File.WriteAllText("..\\..\\output.txt", data);
            }
            catch (IOException ex)
            {
                Console.Write("Error occured when writing data\n" + ex);
            }

            //Deviation
            double avg = sum / cou;
            double sum2 = 0;
            foreach (int i in list) {
                sum2 += Math.Pow( i - avg, 2 );
            }
            double dev = Math.Pow(sum2/cou, 0.5);
            Console.WriteLine("Standart deviation is: {0}", dev);
            Console.ReadKey();


        }
    }
}
