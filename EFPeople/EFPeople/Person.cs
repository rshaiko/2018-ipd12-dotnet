﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFPeople
{
    class Person
    {
        [Key] //@ in Java
        public int PersonId { get; set; }

        [MaxLength(50)]
        public String Name { get; set; }

        public int Age { get; set; }
    }


}
