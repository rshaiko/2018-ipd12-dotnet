﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFPeople
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();


            using (DatabaseContext ctx = new DatabaseContext())
            {
                Person p = new Person { Name = "Jarry", Age = rand.Next(1, 150) };
                ctx.People.Add(p); // persist - schedule Person to be inserted into database
                ctx.SaveChanges();
                Console.WriteLine("Person persisted");
            }

            
            using (DatabaseContext ctx = new DatabaseContext())
            {
                var people = (from r in ctx.People select r).ToList();
                foreach (var pp in people)
                {
                    Console.WriteLine(pp.Name + " " + pp.Age);
                }
            }
            Console.ReadKey();

        }
    }
}
