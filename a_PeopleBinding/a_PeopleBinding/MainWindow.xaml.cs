﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace a_PeopleBinding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {
        static int maxId = 0;
        List<Person> people = new List<Person>();

        public MainWindow()
        {
            InitializeComponent();
            lvPeople.ItemsSource = people;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            if (int.TryParse(tbAge.Text, out int age))
            {
                people.Add(new Person(getNewId(), name, age));
                lvPeople.Items.Refresh();
            }
        }

        static int getNewId()
        {
            return ++maxId;
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1) {
                return;
            }
            string name = tbName.Text;
            int id = int.Parse(lblId.Content.ToString());
            if (int.TryParse(tbAge.Text, out int age))
            {
                Person p = (Person)lvPeople.SelectedValue;
                p.Id = id;
                p.Name = name;
                p.Age = age;
                lvPeople.Items.Refresh();
            }
        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1) {
                return;
            }
            Person p = (Person)lvPeople.SelectedValue;
            lblId.Content = p.Id;
            tbName.Text = p.Name;
            tbAge.Text = p.Age + "";
        }

        private void cmRemove_Click(object sender, RoutedEventArgs e)
        {
            people.RemoveAt(lvPeople.SelectedIndex);
            lvPeople.Items.Refresh();
        }
    }
}
