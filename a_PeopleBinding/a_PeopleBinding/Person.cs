﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace a_PeopleBinding
{
    //public partial class ListViewGridViewSample : Window
    //{
    //    public ListViewGridViewSample()
    //    {
    //        InitializeComponent();
            
    //    }
    //}

    class Person
    {
        int id;
        string name;
        int age;

        public Person(int id, string name, int age)
        {
            Id = id;
            Name = name;
            Age = age;
        }

        public int Id
        {
            get => id;
            set => id = value;
        }
        public string Name
        {
            get => name;
            set => name = value;
        }
        public int Age
        {
            get => age;
            set => age = value;
        }
    }
}
