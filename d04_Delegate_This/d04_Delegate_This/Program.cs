﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d04_Delegate_This
{
    class Program
    {

        static void PrintSimple(string line) {
            Console.WriteLine("Simple: " + line);
        }
        static void PrintFancy(string line) {
            Console.WriteLine("!!! Fancy !!!: " + line);
        }

        static void PrintToFile(String data) {
            File.AppendAllText(@"..\..\..\ouptput.txt", data + "\n");
        }

        // !!!!! Declare the type of reference to a method - the signature of the method //
        // !! Every method that takes String as AccessViolationException parameter and return void
        delegate void PrinterMethodType(String s);

        static void Main(string[] args)
        {
            //Creaing pointer, Pointer has to have TYPE !!!

            PrinterMethodType printer = null;
            printer = PrintSimple; //pointer to method
            // we can: printer = new PrinterMethodType(PrintSimple);
            printer += PrintFancy; // add a pointer
            printer += PrintToFile; // 
            printer -= PrintFancy;
            printer("Hello wia delegate");

            Console.ReadKey();
        }
    }
}
