﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_ToDo
{
    class Database
    {
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\!!!C_Sharp_Rep\c_ToDo\todoDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public int AddToDo(ToDo item)
        {
            string sql = "INSERT INTO todos (Task, DueDate, IsDone) VALUES (@Task, @DueDate, @IsDone);  " +
                "SELECT CONVERT(int, SCOPE_IDENTITY());";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@Task", item.task);
                cmd.Parameters.AddWithValue("@DueDate", item.DueDate);
                cmd.Parameters.AddWithValue("@IsDone", item.IsDone ? 1 : 0);
                int id = (int)cmd.ExecuteScalar();
                return id;
            }
        }

        public List<ToDo> GetAllTodos()
        {
            List<ToDo> result = new List<ToDo>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM Todos", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string task = (string)reader["Task"];
                    DateTime dueDate = (DateTime)reader["DueDate"];
                    bool isDone = (byte)reader["IsDone"]!=0;
                    ToDo item = new ToDo(id, task, dueDate, isDone);
                    result.Add(item);
                }
            }
            return result;
        }


        internal void UpdateToDo(ToDo item)
        {
            string sql = "UPDATE todos SET Task = @Task, DueDate = @DueDate, IsDone = @IsDone WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Id", item.id);
            cmd.Parameters.AddWithValue("@Task", item.task);
            cmd.Parameters.AddWithValue("@DueDate", item.DueDate);
            cmd.Parameters.AddWithValue("@IsDone", item.IsDone ? 1 : 0);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        internal void DeleteToDoById(int id)
        {
            string sql = "DELETE FROM todos WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
    }
}
