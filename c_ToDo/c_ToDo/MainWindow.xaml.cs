﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace c_ToDo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Globals.db = new Database();
            InitializeComponent();
            try
            {
                refresh();
            }
            catch(SqlException er)
            {
                MessageBox.Show(er.Message);
            }
        }

        private void refresh()
        {
            lvTodos.ItemsSource = Globals.db.GetAllTodos();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dlg = new AddEditDialog(null);
            if (dlg.ShowDialog() == true)
            {
                lvTodos.ItemsSource = Globals.db.GetAllTodos();
            }
        }

        private void lvTodos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           ToDo todo =(ToDo) lvTodos.SelectedItem;
            if (todo == null) {
                return;
            }
            AddEditDialog dlg = new AddEditDialog(todo);
            if (dlg.ShowDialog() == true)
            {
                lvTodos.ItemsSource = Globals.db.GetAllTodos();
            }
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            // don't refetch from database every time, use a cashed version instead
            List<ToDo> todoList = Globals.db.GetAllTodos();
            String word = tbSearch.Text;
            if (word != "")
            {
                var result = from t in todoList where t.task.Contains(word) select t;

                todoList = result.ToList();
            }
            lvTodos.ItemsSource = todoList;
        }
    }
}
