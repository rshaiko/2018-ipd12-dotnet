﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace c_ToDo
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {

        private ToDo currentItem;

        public AddEditDialog(ToDo item)
        {
            currentItem = item;

            InitializeComponent();

            if (currentItem == null)
            {
                btsave.Content = "Add new";
            }
            else {
                lblId.Content = item.id + "";
                tbTask.Text = item.task;
                dpDueDate.SelectedDate = item.DueDate;
                cbIsDone.IsChecked = item.IsDone;
            }

        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btsave_Click(object sender, RoutedEventArgs e)
        {
            ToDo todo = currentItem == null ? new ToDo () : currentItem;
            todo.task = tbTask.Text;
            todo.DueDate = (DateTime)dpDueDate.SelectedDate;
            todo.IsDone = (bool)cbIsDone.IsChecked;

            if (currentItem == null)
            {
                //add insert
                
                Globals.db.AddToDo(todo);

            }
            else
            {
                //update
                Globals.db.UpdateToDo(todo);
            }
            DialogResult = true;
        }
    }
}
