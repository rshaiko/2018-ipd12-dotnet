﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_ToDo
{
    public class ToDo
    {
        public ToDo(long id, string task, DateTime dueDate, bool isDone)
        {
            this.id = id;
            this.task = task;
            DueDate = dueDate;
            IsDone = isDone;
        }
        public ToDo()
        {

        }
        public long id { get; set; }
        public String task { get; set; }
        public DateTime DueDate { get; set; }
        public bool IsDone { get; set;}

    }
}
