﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SharpNote
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool areChanges=false;
        string path;
        string filename = "";
        public MainWindow()
        {
            InitializeComponent();
            path = "no file";
            lblPath.Text = path;

        }

        
        private void mOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    path = openFileDialog.FileName;
                    txtEditor.Text = File.ReadAllText(path);
                    areChanges = false;
                    lblPath.Text = path;
                    string[] tmp = path.Split('\\');
                    filename = tmp[tmp.Length - 1];
                    MW.Title = filename +  " - Mini Notepad";
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error opening file: "+ex.Message, "File open error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            
        }

        private void mSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                path = saveFileDialog.FileName;
                File.WriteAllText(saveFileDialog.FileName, txtEditor.Text);
                lblPath.Text = path;
                areChanges = false;
                string[] tmp = path.Split('\\');
                filename = tmp[tmp.Length - 1];
                MW.Title = MW.Title = filename + " - Mini Notepad"; 
            }
            
        }

        private void mExit_Click(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Close();
        }

        private void mSave_Click(object sender, RoutedEventArgs e)
        {
            if (path.Equals("no file"))
            {
                mSaveAs_Click(sender, e);               
            }
            else
            {
                File.WriteAllText(path, txtEditor.Text);
                areChanges = false;
            }
            
        }

        private void txtEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!areChanges)
            {
                areChanges = true;
                lblPath.Text = "*"+path;
            } 
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (areChanges)
            {
                MessageBoxResult result = MessageBox.Show("Would you like to save changes before exit?", "Unsaved changes!", MessageBoxButton.YesNoCancel);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        if (path.Equals("no file"))
                        {
                            SaveFileDialog saveFileDialog = new SaveFileDialog();
                            if (saveFileDialog.ShowDialog() == true)
                            {
                                path = saveFileDialog.FileName;
                                File.WriteAllText(path, txtEditor.Text);
                            }
                            else
                            {
                                e.Cancel = true;
                            }
                        }
                        else
                        {
                            File.WriteAllText(path, txtEditor.Text);
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }

    }
}
