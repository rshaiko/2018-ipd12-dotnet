﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace EPeople
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List <Person> people = new List <Person>();
        DatabaseContext dbc = new DatabaseContext();
        public MainWindow()
        {
            
            InitializeComponent();
            Random rand = new Random();
            Person p = new Person {id=5, name = "Bob", age = rand.Next(1, 150), height = rand.Next(100, 210)};
            dbc.insertEntry(p);
            //people.Add(p);
            lvPeople.ItemsSource = dbc.GetPeople();
            
                
        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1)
            {
                return;
            }
            Person p = (Person)lvPeople.SelectedValue;
            lblId.Content = p.id;
            tbName.Text = p.name;
            tbAge.Text = p.age + "";
            slHeight.Value = p.height;
        }


        private void cmRemove_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1)
            {
                return;
            }
            Person p = (Person)lvPeople.SelectedValue;
            dbc.deletePerson(p.id);
            lvPeople.ItemsSource = dbc.GetPeople();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1)
            {
                return;
            }
            try
            {
                Person p = (Person)lvPeople.SelectedValue;
                p.name = tbName.Text;
                p.height = (int)slHeight.Value;
                p.age = int.Parse(tbAge.Text);
                dbc.updatePerson(p);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            lvPeople.ItemsSource = dbc.GetPeople();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            int height = (int)(slHeight.Value);
            if (int.TryParse(tbAge.Text, out int age))
            {
                try
                {
                    dbc.addPerson(new Person{name = name, age = age, height = height});
                }
                catch (SqlException er)
                {
                    MessageBox.Show(er.Message);
                    return;
                }
            }
            lvPeople.ItemsSource = dbc.GetPeople();
        }

        private void slHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

            {
                try { tfHeight.Text = slHeight.Value.ToString(); }
                catch (NullReferenceException) { }
            }
        }
    }
}
