﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPeople
{
    class DatabaseContext : DbContext
    {
        
        public DatabaseContext() : base("name=PeopleAutoDB")
        {
        }

        public virtual DbSet<Person> People { get; set; }

        internal void insertEntry(Person p)
        {
            People.Add(p); // persist - schedule Person to be inserted into database table
            //SaveChanges(); // flush - force all scheduled operations to be carried out in database
            Console.WriteLine("Person persisted");
        }

        internal IEnumerable GetPeople()
        {
            int id, age, height;
            string name;
          
            List<Person> list = new List<Person>();

            // select all recors
            var people = (from r in People select r).ToList();
            foreach (var pp in people)
            {
                id = pp.id;
                name = pp.name;
                age = pp.age;
                height = pp.height;
                Person p = new Person { id=id, name=name, age=age, height=height };
                list.Add(p);
                //Console.WriteLine("P({0}): {1}, {2}", pp.id, pp.name, pp.age);
            }
            return list;
        }

        internal void deletePerson(int id)
        {
            var people = (from r in People where r.id == id select r).ToList();
            if (people.Count == 0)
            {
                Console.WriteLine("Record for deletion not found");
            }
            else
            {
                Person p = people[0];
                People.Remove(p);
                SaveChanges();
                Console.WriteLine("Deletion method 2 succeeded");
            }
        }

        internal void updatePerson(Person p)
        {
            var people = (from r in People where r.id == p.id select r).ToList();
            if (people.Count == 0)
            {
                Console.WriteLine("Record for update not found");
            }
            else
            {
                Person pp = people[0];
                //pp.id = p.id;
                pp.name = p.name;
                pp.age = p.age;
                pp.height = p.height;
                // entity is tracked/attached, meaning this will cause an update to be scheduled
                SaveChanges();
            }
        }

        internal void addPerson(Person person)
        {
            People.Add(person); // persist - schedule Person to be inserted into database table
            SaveChanges(); // flush - force all scheduled operations to be carried out in database
            Console.WriteLine("Person persisted");
        }
    }
}
