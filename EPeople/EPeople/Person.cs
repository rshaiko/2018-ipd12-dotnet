﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPeople
{
    class Person
    {
        [Key]
        public int id { get; set; }
        [MaxLength(50)]
        public String name { get; set; }
        public int age { get; set; }
        public int height { get; set; }

    }
}
