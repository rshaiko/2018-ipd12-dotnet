﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace b_NewPeopleDB
{
    
    class Database
    {
        public MySqlConnection conn;


        public Database()
        {
            conn = new MySqlConnection("Server=den1.mysql3.gear.host; database=stocktracker; UID=stocktracker; password=Fx4Sq8vm?!zv");
            //conn = new MySqlConnection("Server=localhost; database=first; UID=root; password=root");
            //conn = new SqlConnection(@"server=localhost;user id=root;persistsecurityinfo=True;database=first");

            //conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\People.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public void addPerson (Person p)
        {
            // insert a records
            MySqlCommand insertCommand = new MySqlCommand("INSERT INTO People (name, age, height) VALUES (@Name, @Age, @Height)", conn);
            insertCommand.Parameters.AddWithValue("@Name", p.Name);
            insertCommand.Parameters.AddWithValue("@Age", p.Age);
            insertCommand.Parameters.AddWithValue("@Height", p.Height);
            insertCommand.ExecuteNonQuery();
        }

        public List <Person> GetPeople()
        {
            int id, age;
            string name;
            float height;
            List<Person> list = new List<Person>();

            // select all recors
            MySqlCommand command = new MySqlCommand("SELECT * FROM people", conn);
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    id = int.Parse(reader[0].ToString());
                    name = reader[1].ToString();
                    age = int.Parse(reader[2].ToString());
                    height = float.Parse(reader[3].ToString());
                    Person p = new Person(id, name, age, height);
                    list.Add(p);
                }
            }
            return list;
        }

        internal void deletePerson(int id)
        {
            MySqlCommand cmd = new MySqlCommand("DELETE FROM People WHERE id=@Id", conn);
            cmd.Parameters.AddWithValue("@Id", id);
            cmd.ExecuteNonQuery();
        }

        internal void updatePerson(Person p)
        {
            MySqlCommand insertCommand = new MySqlCommand("UPDATE People SET name=@Name, age=@Age, height=@Height WHERE id = @Id", conn);
            insertCommand.Parameters.AddWithValue("@Id", p.Id);
            insertCommand.Parameters.AddWithValue("@Name", p.Name);
            insertCommand.Parameters.AddWithValue("@Age", p.Age);
            insertCommand.Parameters.AddWithValue("@Height", p.Height);
            insertCommand.ExecuteNonQuery();
        }
    }


}
