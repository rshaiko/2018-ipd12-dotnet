﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace b_NewPeopleDB
{
    class Person
    {
        int id;
        string name;
        int age;
        float height;

        public Person(int id, string name, int age, float height)
        {
            Id = id;
            Name = name;
            Age = age;
            Height = height;
        }
        public Person(string name, int age, float height)
        {
            Name = name;
            Age = age;
            Height = height;
        }

        public int Id
        {
            get => id;
            set => id = value;
        }
        public string Name
        {
            get => name;
            set => name = value;
        }
        public int Age
        {
            get => age;
            set => age = value;
        }
        public float Height
        {
            get => height;
            set => height = value;
        }
    }
}

