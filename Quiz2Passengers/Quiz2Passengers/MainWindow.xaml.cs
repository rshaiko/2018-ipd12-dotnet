﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Globals.db = new Database();
            InitializeComponent();
            refreshPassengerList();

        }

        private void refreshPassengerList()
        {
            lvPassengers.ItemsSource = Globals.db.GetAllPassengers();
        }

        private void btSort_Click(object sender, RoutedEventArgs e)
        {
            //Window1 sortDlg = new Window1();
            //if (sortDlg.ShowDialog() == true) {
            //}
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dlg = new AddEditDialog(null);
            if (dlg.ShowDialog() == true)
            {
                lvPassengers.ItemsSource = Globals.db.GetAllPassengers();
            }
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Passenger> pList = Globals.db.GetAllPassengers();
            String word = tbSearch.Text;
            if (word != "")
            {
                var result = from t in pList where t.Name.Contains(word) || t.Destination.Contains(word) select t;
                pList = result.ToList();
            }
            lvPassengers.ItemsSource = pList;
        }

        private void lvPassengers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Passenger p = (Passenger)lvPassengers.SelectedItem;
            if (p == null)
            {
                return;
            }
            AddEditDialog dlg = new AddEditDialog(p);
            if (dlg.ShowDialog() == true)
            {
                lvPassengers.ItemsSource = Globals.db.GetAllPassengers();
            }
        }
    }
}
