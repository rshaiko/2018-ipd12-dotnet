﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Quiz2Passengers
{
    class Database
    {
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\!!!C_Sharp_Rep\Quiz2Passengers\Quiz2PassengersDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public int AddPassenger(Passenger p)
        {
            string sql = "INSERT INTO Passengers (name, passport, destination, departure) VALUES (@Name, @Passport, @Destination, @Departure);  " +
                "SELECT CONVERT(int, SCOPE_IDENTITY());";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@Name", p.Name);
                cmd.Parameters.AddWithValue("@Passport", p.Passport);
                cmd.Parameters.AddWithValue("@Destination", p.Destination);
                cmd.Parameters.AddWithValue("@Departure", p.Departure);
                int id = (int)cmd.ExecuteScalar();
                return id;
            }
        }

        public List<Passenger> GetAllPassengers()
        {
            List<Passenger> result = new List<Passenger>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM Passengers", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["id"];
                    string name = (string)reader["name"];
                    string passport = (string)reader["passport"];
                    string destination = (string)reader["destination"];
                    DateTime departure = (DateTime)reader["departure"];
                    bool departed;
                    //is departed
                    if(departure< DateTime.Now)
                    {
                        departed = true;
                    }
                    else
                    {
                        departed = false;
                    }
                    Passenger p = new Passenger (id, name, passport, destination, departure, departed);
                    result.Add(p);
                }
            }
            return result;
        }

        internal void UpdatePassenger(Passenger p)
        {
            string sql = "UPDATE Passengers SET name = @Name, passport = @Passport, destination = @Destination, departure = @Departure  WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Id", p.Id);
            cmd.Parameters.AddWithValue("@Name", p.Name);
            cmd.Parameters.AddWithValue("@Passport", p.Passport);
            cmd.Parameters.AddWithValue("@Destination", p.Destination);
            cmd.Parameters.AddWithValue("@Departure", p.Departure);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        internal void DeletePassengerById(int id)
        {
            string sql = "DELETE FROM Passengers WHERE id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
    }
}
