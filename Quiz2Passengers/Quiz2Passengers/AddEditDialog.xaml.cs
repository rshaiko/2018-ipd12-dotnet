﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        private Passenger currentP;

        public AddEditDialog(Passenger p)
        {
            currentP = p;
            InitializeComponent();
            if (currentP == null)
            {
                btSaveAdd.Content = "Add";
                btDelete.IsEnabled = false;
            }
            else
            {
                btSaveAdd.Content = "Save";
                lblId.Content = p.Id + "";
                tbName.Text = p.Name;
                tbPassport.Text = p.Passport;
                tbDestination.Text = p.Destination;
                dpDepDate.SelectedDate = p.Departure;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btSaveAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string passport = tbPassport.Text;
            string destination = tbDestination.Text;
            DateTime departure = (DateTime)dpDepDate.SelectedDate;
           

            if (btSaveAdd.Content.ToString() == "Add")
            {
                //adding new
                Passenger p = new Passenger(name, passport, destination, departure, false);
                try
                {
                    Globals.db.AddPassenger(p);
                }
                catch (SqlException er)
                {
                    MessageBox.Show("Database error. Entry was not added! \n"+ er.Message);
                    return;
                }
            }
            else
            {
                //update
                int id = int.Parse(lblId.Content.ToString());
                Passenger p = new Passenger(id, name, passport, destination, departure, false);
                try
                {
                    Globals.db.UpdatePassenger(p);
                }
                catch (SqlException er)
                {
                    MessageBox.Show("Database error. Entry was not updated! \n" + er.Message);
                    return;
                }
            }
            DialogResult = true;
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(lblId.Content.ToString());
            try
            {
                Globals.db.DeletePassengerById(id);
            }
            catch (SqlException er)
            {
                MessageBox.Show("Database Error. Entry was not deleted! \n" + er.Message);
                return;
            }
            DialogResult = true;
        }
    }
}
