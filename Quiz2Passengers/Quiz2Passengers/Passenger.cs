﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2Passengers
{
    public class Passenger
    {
        public Passenger(int id, string name, string passport, string destination, DateTime departure, bool departed)
        {
            Id = id;
            Name = name;
            Passport = passport;
            Destination = destination;
            Departure = departure;
            Departed = departed;
        }

        //for adding
        public Passenger(string name, string passport, string destination, DateTime departure, bool departed)
        {
            Name = name;
            Passport = passport;
            Destination = destination;
            Departure = departure;
            Departed = departed;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Passport { get; set; }
        public string Destination { get; set; }
        public DateTime Departure { get; set; }
        public bool Departed { get; set; }
    }
}
