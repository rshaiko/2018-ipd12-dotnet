﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d04_SortEaasy
{
    class Person {
        public string Name;
        public int Age;
    }


    class Program
    {

        static void Main(string[] args)
        {
            List <Person> list = new List <Person>();
            list.Add(new Person() {Name = "Jerry", Age = 37 });
            list.Add(new Person() { Name = "Mary", Age = 25 });
            list.Add(new Person() { Name = "Bobby", Age = 42 });
            list.Add(new Person() { Name = "Martin", Age = 55 });

            foreach (Person p in list) {
                Console.WriteLine("Name: {0} \tAge: {1}", p.Name, p.Age);
            }

            //list by Name
            

            Console.ReadKey();
        }
    }
}
